import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as mat_pyplot
import json
import urllib.request
import matplotlib.pyplot
import matplotlib.dates
import datetime


class ConvertArgumentTypes(object):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def __call__(self, input_func):
        def func(*args, **kwargs):
            zip_args = [val[0](val[1]) for val in zip(self.args, args)]
            invalid_kwargs = [kw for kw in kwargs if kw not in self.kwargs]
            if len(invalid_kwargs) > 0:
                raise TypeError(input_func.func_name + "() got an unexpected keyword argument '%s'" % invalid_kwargs[0])
            kwargs = dict([(x, self.kwargs[x](kwargs[x])) for x in kwargs])
            value = input_func(*zip_args, **kwargs)
            return value

        return func


class PlotError(Exception):
    pass


@ConvertArgumentTypes(str, int, int, str, str)
def history(crypt_from: str, from_date: int, to_date: int, range: str, crypt_to: str):
    api_result = {'minute': 1, 'hour': 60, 'day': 1440}
    interval_lim = (to_date - from_date) // (api_result[range] * 60)
    query = 'https://min-api.cryptocompare.com/data/histo' + range + '?fsym=' + crypt_from + '&tsym=' + crypt_to + '&interval_lim=' + str(
        interval_lim) + '&toTs=' + str(to_date)
    answer_query = json.loads(urllib.request.urlopen(query).read().decode('utf-8'))
    values_mat = list(map(lambda x: x['close'], answer_query['Data']))
    if (values_mat is False):
        raise KeyError
    dates_mat = matplotlib.dates.date2num(
        list(map(lambda x: datetime.datetime.fromtimestamp(x['time']), answer_query['Data'])))
    mat_pyplot.plot_date(dates_mat, values_mat, '-o')
    mat_pyplot.gcf().autofmt_xdate()
    mat_pyplot.scatter(dates_mat, values_mat)
    mat_pyplot.title(crypt_from + ' currency')
    mat_pyplot.xlabel('Date')
    mat_pyplot.ylabel(crypt_from + ' to ' + crypt_to)
    mat_pyplot.savefig('graph_picture.png')
    mat_pyplot.close()


@ConvertArgumentTypes(str, str)
def crate(crypt_from='EUR', crypt_to='RUB'):
    answer_query = json.loads(
        urllib.request.urlopen(
            'https://min-api.cryptocompare.com/data/price?fsym=' + crypt_from + '&tsyms=' + crypt_to).read().decode(
            'utf-8'))
    return json.dumps(answer_query)
