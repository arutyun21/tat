from telegram.ext import Updater, CommandHandler
import cryptic_parser
import logging
from dateutil import parser as date_parser
import time
import os

with open('readme.txt', 'r', encoding='utf8') as input_file:
    start_text = input_file.read()


def run_bot(token):  # запуск бота
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO)
    updater = Updater(token=token)
    dispatcher = updater.dispatcher  # создание диспетчера и его дальнейшее заполнение handler'ами
    start_handler = CommandHandler('start', start)
    crate_handler = CommandHandler('crate', bot_crate)
    history_handler = CommandHandler('history', bot_history)
    help_handler = CommandHandler('help', bot_help)
    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(crate_handler)
    dispatcher.add_handler(history_handler)
    dispatcher.add_handler(help_handler)
    updater.start_polling()


def bot_history(bot,
                update):  # вывод графика изменения курса валюты в указанном временом промежутке, с указанным шагом,
    try:
        message_text = update.message.text.split()
        del message_text[0]
        date_to = time.mktime(date_parser.parse(message_text[2]).timetuple())
        date_from = time.mktime(date_parser.parse(message_text[1]).timetuple())
        cryptic_parser.history(message_text[0], int(date_from), int(date_to), *message_text[3:])
        if (date_to < date_from):
            bot.send_message(chat_id=update.message.chat_id, text='Ошибка ввода времени!')
        elif (date_parser.parse(message_text[1]).timetuple()[0] < 2008 or
              date_parser.parse(message_text[2]).timetuple()[0] < 2008 or
              date_parser.parse(message_text[1]).timetuple()[0] > 2018 or
              date_parser.parse(message_text[2]).timetuple()[0] > 2018):
            bot.send_message(chat_id=update.message.chat_id,
                             text='В базе нет актуальной информации в данном промежутке времени.'
                                  'Попробуйте ввести время укладывающееся в следующий промежуток [2008 , 2018]')
        else:
            bot.send_photo(chat_id=update.message.chat_id, photo=open('graph_picture.png', 'rb'))
            os.remove('./graph_picture.png')
    except IndexError:
        bot.send_message(chat_id=update.message.chat_id,
                         text='Формат ввода имеет следующий вид: /history < аргументы >.'
                              'Проверьте правильность ввода аргументов.')
    except KeyError:
        bot.send_message(chat_id=update.message.chat_id, text='Проверьте правильность ввода данных.')


def bot_help(bot, update):
    bot.send_message(chat_id=update.message.chat_id,
                     text="Команды: /crate - курс обмена валюты, аргументы: crypt_from, crypt_to; "
                          "/start - запустить бота; "
                          "/history - вывод графика изменения курса валюты в указанном временом промежутке, с указанным шагом, "
                          "аргументы: crypt_from, date_from, date_to, range, crypt_to "
                     )


def bot_crate(bot, update):  # возвращает курс введенной валюты(по умолчанию евро - доллар)
    bot.send_message(
        chat_id=update.message.chat_id,
        text=cryptic_parser.crate(*update.message.text.split()[1:]))


def start(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text=start_text)
